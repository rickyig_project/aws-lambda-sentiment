import re
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import nltk
import pickle
import joblib

port_stem = PorterStemmer()

def stemming(content):
    stemmed_content = re.sub('[^a-zA-Z]', " ", content)   # the regular expression matches any pattern that is not a character
                                                          # (since negation ^ is used) and replaces those matched sequences 
                                                          # with empty space, thus all special characters and digits get 
                                                          # removed.
    stemmed_content = stemmed_content.lower()
    stemmed_content = stemmed_content.split()
    stemmed_content = [port_stem.stem(word) for word in stemmed_content if word not in stopwords.words('english')]   
                                                          # apply port_stem only on words not in the list of stop-words
    stemmed_content = " ".join(stemmed_content)
    
    return stemmed_content

# vectorizer = pickle.load(open('assets/vectorizer.pickle', 'rb'))
vectorizer = joblib.load('assets/TF-IDF_v1.0.0.joblib')

def preprocess(text):
    stemmed = stemming(text)
    vector = vectorizer.transform([stemmed])
    return vector

if __name__ == '__main__':
    print(preprocess('that food is so good'))