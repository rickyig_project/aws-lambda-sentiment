from fastapi import FastAPI
from joblib import load
from preprocess import preprocess  # Assuming preprocess is a custom module you've created

app = FastAPI()

# Load your model
model = load('assets/Logistic Regression_v1.0.0.joblib')
classes = ['negative', 'positive']

#setup routing untuk endpointnya dengan metode get
@app.get("/")
async def predict(text: str):
    vector = preprocess(text)
    preds = model.predict(vector)[0]
    prediction = classes[preds]
    return {"text": text, "prediction": prediction}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
