from fastapi import FastAPI
from joblib import load
from preprocess import preprocess  # Assuming preprocess is a custom module you've created
from datetime import datetime

app = FastAPI()

# Load your model
model = load('assets/Logistic Regression_v1.0.0.joblib')
classes = ['negative', 'positive']

# Define a global uncertainty threshold
uncertainty_threshold = 0.6  # Adjust as needed

@app.get("/")
async def predict(text: str):
    vector = preprocess(text)
    probs = model.predict_proba(vector)[0]  # Get prediction probabilities
    
    # Determine the index of the maximum probability (predicted class)
    pred_index = probs.argmax()
    prediction = classes[pred_index]
    confidence = probs[pred_index]
    
    # Determine if the prediction is uncertain
    is_uncertain = confidence < uncertainty_threshold
    is_uncertain = bool(is_uncertain)  # Explicit conversion to Python bool
    
    # Get the current timestamp and format it
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    return {
        "text": text,
        "prediction": prediction,
        "confidence": float(confidence),  # Convert to Python float for JSON serialization
        "is_uncertain": is_uncertain,
        "timestamp": timestamp  # Include the formatted timestamp in the response
    }

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
