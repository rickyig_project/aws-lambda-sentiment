from fastapi import FastAPI
from joblib import load
from datetime import datetime
from mangum import Mangum
import boto3
import uuid

import re
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import nltk
import pickle
nltk.data.path.append("nltkdata")


port_stem = PorterStemmer()

def stemming(content):
    stemmed_content = re.sub('[^a-zA-Z]', " ", content)   # the regular expression matches any pattern that is not a character
                                                          # (since negation ^ is used) and replaces those matched sequences 
                                                          # with empty space, thus all special characters and digits get 
                                                          # removed.
    stemmed_content = stemmed_content.lower()
    stemmed_content = stemmed_content.split()
    stemmed_content = [port_stem.stem(word) for word in stemmed_content if word not in stopwords.words('english')]   
                                                          # apply port_stem only on words not in the list of stop-words
    stemmed_content = " ".join(stemmed_content)
    
    return stemmed_content

vectorizer = load('assets/TF-IDF_v1.0.0.joblib')

def preprocess(text):
    stemmed = stemming(text)
    vector = vectorizer.transform([stemmed])
    return vector

dynamodb = boto3.resource('dynamodb') 
app = FastAPI()
handler=Mangum(app)

# Load your model
model = load('assets/Logistic Regression_v1.0.0.joblib')
classes = ['negative', 'positive']

# Define a global uncertainty threshold
uncertainty_threshold = 0.5  # Adjust as needed

def upload_data_to_dynamodb(data, table_name):
    table = dynamodb.Table(table_name)
    response = table.put_item(
        Item={ 
            'id': str(uuid.uuid4()),
            'timestamp': data['timestamp'], 
            'text': data['text'], 
            'prediction': data['prediction'], 
            'confidence': str(data['confidence']), 
        } 
    )
    return response

@app.get("/")
async def predict(text: str):
    vector = preprocess(text)
    probs = model.predict_proba(vector)[0]  # Get prediction probabilities
    
    # Determine the index of the maximum probability (predicted class)
    pred_index = probs.argmax()
    prediction = classes[pred_index]
    confidence = probs[pred_index]
    
    # Determine if the prediction is uncertain
    is_uncertain = confidence < uncertainty_threshold
    is_uncertain = bool(is_uncertain)  # Explicit conversion to Python bool
    
    # Get the current timestamp and format it
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    result = {
        "text": text,
        "prediction": prediction,
        "confidence": float(confidence),  # Convert to Python float for JSON serialization
        "is_uncertain": is_uncertain,
        "timestamp": timestamp  # Include the formatted timestamp in the response
    }
    
    response = upload_data_to_dynamodb(result, 'uncertainty')
    return result

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=9000)
